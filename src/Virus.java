import java.util.*;

/**
 * A virus exists randomly attacks only rabbits in the field, which die when it
 * finds them. Only one virus will ever exist in the field.
 * 
 * @author Jori Gelbaugh
 * @version 1/27/2016
 */

public class Virus extends Actor {
	// The location in the field
	private Location location;
	// The current field
	private Field currentField;
	// The updated field
	private Field updatedField;
	// Variable that stores if virus is alive/not
	private boolean alive;

	/**
	 * Constructor for objects of class Virus
	 */
	public Virus(Field currentField, Field updatedField) {
		this.currentField = currentField;
		this.updatedField = updatedField;
	}


	/**
	 * The virus randomly spawns in rabbits.
	 * 
	 * @param currentField and updatedField
	 */
	public void act(Field currentField, Field updatedField) {
		virusOutbreak(currentField, updatedField);
	}

	/**
	 * The virus will randomly materialize in a location of the field. If
	 * there's an animal in the spot where it appears, the animal will die.
	 * 
	 * @param current
	 *            field and updated field.
	 */
	private void virusOutbreak(Field currentField, Field updatedField) {
		Location newOne = updatedField.freeAdjacentLocation(getLocation());
		if (newOne != null) {
			// Creates a virus
			Virus newVirus = new Virus(currentField, updatedField);
			newVirus.setLocation(newOne);
			updatedField.place(newVirus);
			alive = true;
		}

		// Finds a rabbit and kills it
			Location randomLocation = currentField.getRandomLocation();
			Object actor = updatedField.freeAdjacentLocation(getLocation());
			if (actor instanceof Rabbit) {
				((Rabbit) actor).setDead();
			}

			Location newLocation = findRabbit(currentField, getLocation());
			if (newLocation == null) { // no rabbit found - move randomly
				newLocation = updatedField.freeAdjacentLocation(getLocation());
			}
			if (newLocation != null) {
				setLocation(newLocation);
				updatedField.place(this); // sets location
			}
		}
	//}

	/**
	 * Finds a rabbit in the field and sets the one in the location to dead if
	 * there is
	 * 
	 * @param field
	 * @param location
	 * @return Location
	 */
	private Location findRabbit(Field field, Location location) {
		Iterator<Location> adjacentLocations = field.adjacentLocations(location);
		while (adjacentLocations.hasNext() && alive == true) {
			Location where = adjacentLocations.next();
			Object actor = field.getObjectAt(where);
			if (actor instanceof Rabbit) {
				Rabbit rabbit = (Rabbit) actor;
				if (rabbit.isAlive()) {
					rabbit.setDead();
					return where;
				}
			}
		}
		return null;
	}
	
	 /**
     * Tell the virus that it's dead
     */
    public void setDead()
    {
        alive = false;
    }
    
    /**
     * Check whether the animal is alive or not.
     * @return True if the animal is still alive.
     */
    public boolean getAlive()
    {
        return alive;
    }
}
