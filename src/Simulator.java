import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.awt.Color;

// Added the following lines
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * A simple predator-prey simulator, based on a field containing rabbits and
 * foxes.
 * 
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 * @modified Chuck Cusack, September, 2007
 * 
 */
public class Simulator {
	// Constants representing configuration information for the simulation.
	// The default width for the grid.
	private static final int DEFAULT_WIDTH = 50;
	// The default depth of the grid.
	private static final int DEFAULT_DEPTH = 50;
	// The probability that a fox will be created in any given grid position.
	private static final double FOX_CREATION_PROBABILITY = 0.03;
	// The probability that a rabbit will be created in any given grid position.
	private static final double RABBIT_CREATION_PROBABILITY = 0.09;
	// The probability that a virus will be created in any given grid position.
	private static final double VIRUS_CREATION_PROBABILITY = 0.01;

	// The current state of the field.
	private Field currentField;
	// A second field, used to build the next stage of the simulation.
	private Field updatedField;
	// The current step of the simulation.
	private int step;
	// A graphical view of the simulation.
	private SimulatorView view;

	// Added this variable for use by the thread.
	private int numberSteps;

	// Added the following GUI stuff
	// The main window to display the simulation (and your buttons, etc.).
	private JButton runOneButton;
	private JButton run100Button;
	private JButton runLongButton;
	private JButton resetButton;
	private JButton goButton;
	private JFrame mainFrame;
	private JTextField textBox;
	
	// Creates the menu bar
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem menuItem;

	/**
	 * Construct a simulation field with default size.
	 */
	public Simulator() {
		this(DEFAULT_DEPTH, DEFAULT_WIDTH, null);
	}

	public static void main(String[] args) {
		// Create the simulator
		Simulator s = new Simulator();
	}

	/**
	 * Create a simulation field with the given size.
	 * @param depth
	 *            Depth of the field. Must be greater than zero.
	 * @param width
	 *            Width of the field. Must be greater than zero.
	 */
	public Simulator(int depth, int width, Graphics g) {
		if (width <= 0 || depth <= 0) {
			System.out.println("The dimensions must be greater than zero.");
			System.out.println("Using default values.");
			depth = DEFAULT_DEPTH;
			width = DEFAULT_WIDTH;
		}
		currentField = new Field(depth, width);
		updatedField = new Field(depth, width);

		// Create a view of the state of each location in the field.
		view = new SimulatorView(depth, width);
		view.setColor(Rabbit.class, Color.orange);
		view.setColor(Fox.class, Color.blue);
		view.setColor(Virus.class, Color.cyan);
		view.setColor(Doctor.class, Color.black);
		view.getGraphics();
		

		// The rest of this method has changed. This sets up the
		// JFrame to display everything.

		mainFrame = new JFrame();

		mainFrame.setTitle("Fox and Rabbit Simulation");

		// Sets up the text box
		textBox = new JTextField("", 10);

		// Add window listener so it closes properly.
		// when the "X" in the upper right corner is clicked.
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		// Add a button to run 1 steps.
		runOneButton = new JButton("Run 1 step");
		runOneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulateOneStep();
			}
		});

		// Add a button to run a simulation that's 100 steps.
		run100Button = new JButton("Run 100 steps");
		run100Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				run100Simulation();
			}
		});

		// Add a button to run a long simulation.
		runLongButton = new JButton("Run a long simulation");
		runLongButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runLongSimulation();
			}
		});

		// Add a button to reset the simulation.
		resetButton = new JButton("Reset simulation");
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reset();
			}
		});

		// Adds a go button that will listen to the text
		goButton = new JButton("Go");
		goButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulate(setUserSteps());
			}

		});
		
		//Adds menu and menu items
		menuBar = new JMenuBar();
		JMenu file = new JMenu("File");
		menuItem = new JMenuItem("Exit");
		file.add(menuItem);
		menuBar.add(file);
		menuItem.addActionListener(new ActionListener() {
			//exits the program
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		//Adds a help menu with an about item
		JMenu help = new JMenu("Help");
		menuItem = new JMenuItem("About");
		help.add(menuItem);
		menuBar.add(help);
		menuItem.addActionListener(new ActionListener() {
			//gives information about the program
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(mainFrame,
						"Enter a number and click go to set the steps.");
			}
		});

		// Adds things to the main frame and to the contents panel
		Container contents = mainFrame.getContentPane();
		JPanel contentsPanel = new JPanel();
		mainFrame.setJMenuBar(menuBar);
		contents.add(view, BorderLayout.CENTER);
		contentsPanel.add(runOneButton);
		contentsPanel.add(run100Button);
		contentsPanel.add(runLongButton);
		contentsPanel.add(textBox);
		contentsPanel.add(goButton);
		contentsPanel.add(resetButton);
		contents.add(contentsPanel, BorderLayout.NORTH);

		// Calls pack on JFrame
		mainFrame.pack();

		// resets the simulation
		reset();

		// You always need to call setVisible(true) on a JFrame.
		mainFrame.setVisible(true);
	}

	/**
	 * Sets the user steps
	 */
	private int setUserSteps() {
		String userText = textBox.getText();
		try {
			numberSteps = Integer.parseInt(userText);
		} catch (Exception e) {
			// sends an error message if text isn't number
			JOptionPane.showMessageDialog(mainFrame, "You must enter a number!", "Inane error",
					JOptionPane.WARNING_MESSAGE);
			// sets the number of steps to 0
			numberSteps = 0;
		}
		return numberSteps;
	}

	/**
	 * Run the simulation from its current state for 100 steps.
	 */
	public void run100Simulation() {
		simulate(100);
	}

	/**
	 * Run the simulation from its current state for a reasonably long period,
	 * e.g. 500 steps.
	 */
	public void runLongSimulation() {
		simulate(500);
	}

	/**
	 * Run the simulation from its current state for the given number of steps.
	 * Stop before the given number of steps if it ceases to be viable. This has
	 * been modified so it uses a thread--this allows it to work in conjunction
	 * with Swing. Modified by Chuck Cusack, Sept 18, 2007
	 * 
	 * @param numSteps
	 *            How many steps to run for.
	 */
	public void simulate(int numSteps) {
		// For technical reason, I had to add numberSteps as a class variable.
		setNumberSteps(numSteps);
		// Create a thread
		Thread runThread = new Thread() {
			// When the thread runs, it will simulate numberSteps steps.
			public void run() {
				// Disable the run one button until the simulation is done.
				runOneButton.setEnabled(false);
				run100Button.setEnabled(false);
				runLongButton.setEnabled(false);
				goButton.setEnabled(false);
				resetButton.setEnabled(false);
				for (int step = 1; step <= getNumberSteps() && view.isViable(currentField); step++) {
					simulateOneStep();
				}

				// Re-enable the go button
				goButton.setEnabled(true);
				// Re-enable the run one button
				runOneButton.setEnabled(true);
				// Re-enable the run long button
				runLongButton.setEnabled(true);
				// Re-enable the run 100 button
				run100Button.setEnabled(true);
				// Re-enable the reset button
				resetButton.setEnabled(true);
			}
		};
		// Start the thread
		runThread.start();
		// Now this method exits, allowing the GUI to update. The simulation is
		// being
		// run on a different thread, so the GUI updates as the simulation
		// continues.
	}

	/**
	 * Run the simulation from its current state for a single step. Iterate over
	 * the whole field updating the state of each fox and rabbit.
	 */
	public void simulateOneStep() {
		step++;

		// let all animals act
		ArrayList<Actor> actors = currentField.getActors();
		Collections.shuffle(actors); // to randomize the order they act.
		for (Iterator<Actor> it = actors.iterator(); it.hasNext();) {
			Actor actor = it.next();
			actor.act(currentField, updatedField);
		}

		// Swap the field and updatedField at the end of the step.
		Field temp = currentField;
		currentField = updatedField;
		updatedField = temp;
		updatedField.clear();

		// Display the new field on screen.
		view.showStatus(step, currentField);
	}

	/**
	 * Reset the simulation to a starting position.
	 */
	public void reset() {
		step = 0;
		currentField.clear();
		updatedField.clear();
		populate(currentField);

		// Show the starting state in the view.
		view.showStatus(step, currentField);
	}

	/**
	 * Populate a field with foxes and rabbits.
	 * 
	 * @param field
	 *            The field to be populated.
	 */
	private void populate(Field field) {
		Random rand = new Random();
		field.clear();
		int doctorCount = 0;
		for (int row = 0; row < field.getDepth(); row++) {
			for (int col = 0; col < field.getWidth(); col++) {
				if(doctorCount == 0){
					Doctor doctor = new Doctor(currentField, updatedField);
					doctor.setLocation(row, col);
					field.place(doctor);
					doctorCount++;
				}
				if (rand.nextDouble() <= VIRUS_CREATION_PROBABILITY){
					Virus virus = new Virus(currentField, updatedField);
					virus.setLocation(row, col);
					field.place(virus);
				}
				if (rand.nextDouble() <= FOX_CREATION_PROBABILITY) {
					Fox fox = new Fox(true);
					fox.setLocation(row, col);
					field.place(fox);
				} else if (rand.nextDouble() <= RABBIT_CREATION_PROBABILITY) {
					Rabbit rabbit = new Rabbit(true);
					rabbit.setLocation(row, col);
					field.place(rabbit);
				}

				// else leave the location empty.
			}
		}

	}

	/**
	 * Gets the number of steps
	 * @return numberSteps
	 */
	public int getNumberSteps() {
		return numberSteps;
	}

	/**
	 * Sets the number of steps
	 * @param numberSteps
	 */
	public void setNumberSteps(int numberSteps) {
		this.numberSteps = numberSteps;
	}

}
