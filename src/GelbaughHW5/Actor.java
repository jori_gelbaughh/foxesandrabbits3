import java.util.List;

/**
 * The interface to be implemented by any class involved in the simulation
 * 
 * @author Jori Gelbaugh
 * @version 1/26/2017
 */
public abstract class Actor
{
	// The animal's position
    private Location location;
    
	/**
     * It's what actors do - act.
     * @param newActors A list into which any newly created Actors are placed
     */
    abstract public void act(Field currentField, 
            Field updatedField);
    
    public Location getLocation()
    {
        return location;
    }

    /**
     * Set the animal's location.
     * @param row The vertical coordinate of the location.
     * @param col The horizontal coordinate of the location.
     */
    public void setLocation(int row, int col)
    {
        this.location = new Location(row, col);
    }

    /**
     * Set the animal's location.
     * @param location The animal's location.
     */
    public void setLocation(Location location)
    {
        this.location = location;
    }
}
