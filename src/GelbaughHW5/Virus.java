import java.util.*;

/**
 * A virus exists randomly attacks only rabbits in the field, which
 * die when it finds them. Only one virus will ever exist in the field.
 * 
 * @author Jori Gelbaugh
 * @version 1/27/2016
 */

public class Virus extends Actor {
	// The location in the field
	private Location location;
	// The current field
	private Field currentField;
	// The updated field
	private Field updatedField;
	// Variable that stores if animal is alive/not
	private boolean alive;

	/**
	 * Constructor for objects of class Virus
	 */
	public Virus(Field currentField, Field updatedField) {
		this.currentField = currentField;
		this.updatedField = updatedField;
	}

	/**
	 * Check whether the animal is alive or not.
	 * 
	 * @return true if the animal is still alive.
	 */
	public boolean isAlive() {
		return true;
	}

	/**
	 * The virus randomly spawns in rabbits.
	 * 
	 * @param newAnimals
	 *            A list to return new animals.
	 */
	public void act(Field currentField, Field updatedField) {
		virusOutbreak(currentField, updatedField);
	}

	/**
     * The virus will randomly materialize in a location of the field.
     * If there's an animal in the spot where it appears, the animal will die.
     * @param current field and updated field.
     */
    private void virusOutbreak(Field currentField, Field updatedField)
    {
    	Location newOne = updatedField.freeAdjacentLocation(getLocation());
		if (newOne != null) {
			// Only create the virus if there is somewhere to put it.
            Virus newVirus = new Virus(currentField, updatedField);
			newVirus.setLocation(newOne);
			updatedField.place(newVirus);
			Location randomLocation = currentField.getRandomLocation();
	        Object actor = updatedField.freeAdjacentLocation(getLocation());
	        if(actor instanceof Rabbit)
	        {
	            ((Rabbit) actor).setDead();
	        }
		}
    	
    }
    }
