import java.util.*;

/**
 * A hunter lives forever and does not eat or breed. He's "blind" in the sense
 * that he randomly fires at Animals in the field; if he hits one, the animal
 * dies. He's in a "helicopter", so he doesn't actually have a location or
 * occupy space in the field.
 * 
 * @author Jori Gelbaugh
 * @version 12/3/2016
 */
public class Doctor extends Actor {
	// The location in the field
	private Location location;
	// The current field
	private Field currentField;
	// The updated field
	private Field updatedField;

	/**
	 * Constructor for objects of class Virus
	 */
	public Doctor(Field currentField, Field updatedField) {
		this.currentField = currentField;
		this.updatedField = updatedField;
	}

	/**
	 * The doctor randomly kills viruses
	 * 
	 * @param newAnimals
	 *            A list to return new animals.
	 */
	public void act(Field currentField, Field updatedField) {
		Location newOne = updatedField.getRandomLocation();
		setLocation(newOne);
		updatedField.place(this);;
		killVirus(updatedField, newOne);
	}

	/**
	 * The doctor will enter a location of the field. If there's a virus where
	 * it appears, the virus will die.
	 * 
	 * @param current
	 *            field and updated field.
	 */
	private void killVirus(Field field, Location location) {
		Actor actor = field.getActorAt(location);
		if(actor instanceof Animal && location.getRow() == getLocation().getRow()){
			((Animal) actor).setDead();
		}
	}
}
